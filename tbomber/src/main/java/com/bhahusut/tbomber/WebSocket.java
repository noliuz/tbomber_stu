package com.bhahusut.tbomber;

import android.util.Log;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;


/**
 * Created by nol on 27/3/2558.
 */
public class WebSocket {
    String server_uri = "ws://10.0.2.2:3000";
    WebSocketClient wsc;

    WebSocket() {

        URI uri;
        try {
            uri = new URI(server_uri);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }

        wsc = new WebSocketClient(uri) {

            @Override
            public void onOpen(ServerHandshake serverHandshake) {
                Log.e("ws", "opened");
                wsc.send("{\"token\":\"0001\"}");
            }

            @Override
            public void onMessage(String s) {
                Log.e("ws","msg come:"+s);

            }

            @Override
            public void onClose(int i,String s,boolean b) {
                Log.e("ws", "closed");
            }

            @Override
            public void onError(Exception e) {
                Log.e("ws","error:"+e.getMessage());
            }

        };
        wsc.connect();
    }

}
